BUILDDIR = build
SRCDIR = src
STUFFDIR = src/stuff
ASMDIR = src/asm

# Primary build target
all: disk.img
	cp $(BUILDDIR)/disk.img ./disk.img

clean:
	rm -rf $(BUILDDIR)
	rm -f ./disk.img

disk.img: disk.img-text res
	dd if=$(BUILDDIR)/main of=$(BUILDDIR)/disk.img bs=512 count=1000 conv=notrunc seek=2

res: main kernel interrupt multitask proc print
	ld -melf_i386 --oformat=binary  -Tlinker.ld \
		 -o $(BUILDDIR)/main  $(BUILDDIR)/kernel.o $(BUILDDIR)/interrupt.o $(BUILDDIR)/print.o $(BUILDDIR)/multitask.o  $(BUILDDIR)/proc.o  $(BUILDDIR)/main.o

multitask:
	gcc -m32 --std=c99 -nostdlib -nodefaultlibs -nostartfiles -c \
		  -o $(BUILDDIR)/multitask.o $(SRCDIR)/multitask.c

proc:
	gcc -m32 --std=c99 -nostdlib -nodefaultlibs -nostartfiles -c \
		-o $(BUILDDIR)/proc.o $(SRCDIR)/proc.c

print:
	gcc -m32 --std=c99 -nostdlib -nodefaultlibs -nostartfiles -c \
		-o $(BUILDDIR)/print.o $(SRCDIR)/print.c

interrupt: $(ASMDIR)/interrupt.asm
	nasm -f elf -o $(BUILDDIR)/interrupt.o $(ASMDIR)/interrupt.asm

main: $(SRCDIR)/main.c
	gcc -m32 --std=c99 -nostdlib -nodefaultlibs -nostartfiles -c \
		  -o $(BUILDDIR)/main.o $(SRCDIR)/main.c

kernel: $(ASMDIR)/kernel.asm
	nasm -f elf -o $(BUILDDIR)/kernel.o $(ASMDIR)/kernel.asm

disk.img-text: disk.img-boot
	dd if=$(STUFFDIR)/text of=$(BUILDDIR)/disk.img bs=512 count=1 conv=notrunc seek=1

disk.img-boot: disk.img-raw boot
	dd if=$(BUILDDIR)/boot of=$(BUILDDIR)/disk.img bs=512 count=1 conv=notrunc

boot: $(ASMDIR)/boot.asm
	nasm -f bin -o $(BUILDDIR)/boot $(ASMDIR)/boot.asm

disk.img-raw:
	mkdir -p $(BUILDDIR)
	dd if=/dev/zero of=$(BUILDDIR)/disk.img bs=1M count=1
