#include "print.h"
#include "multitask.h"

#ifndef IDT
#define IDT

#define PIC_MASTER 0x20
#define PIC_SLAVE 0xA0
#define PIC_MASTER_COMMAND PIC_MASTER
#define PIC_MASTER_DATA (PIC_MASTER+1)
#define PIC_SLAVE_COMMAND PIC_SLAVE
#define PIC_SLAVE_DATA (PIC_SLAVE+1)

#define PIC1		0x20		/* IO base address for master PIC */
#define PIC2		0xA0		/* IO base address for slave PIC */
#define PIC1_COMMAND	PIC1
#define PIC1_DATA	(PIC1+1)
#define PIC2_COMMAND	PIC2
#define PIC2_DATA	(PIC2+1)

#define ICW1_INIT 0x10
#define ICW1_ICW4 0x01
#define NEW_OFFSET 0x20
#define NEW_OFFSET_2 0x28
#define IV_4_8086 0x01

#define ICW1_ICW4	0x01		/* ICW4 (not) needed */
#define ICW1_SINGLE	0x02		/* Single (cascade) mode */
#define ICW1_INTERVAL4	0x04		/* Call address interval 4 (8) */

#define ICW4_8086	0x01		/* 8086/88 (MCS-80/85) mode */
#define ICW4_AUTO	0x02		/* Auto (normal) EOI */
#define ICW4_BUF_SLAVE	0x08		/* Buffered mode/slave */
#define ICW4_BUF_MASTER	0x0C		/* Buffered mode/master */
#define ICW4_SFNM	0x10		/* Special fully nested (not) */

#define IDT_ENTRIES 0x31

extern void isr0();
extern void isr1();
extern void isr2();
extern void isr3();
extern void isr4();
extern void isr5();
extern void isr6();
extern void isr7();
extern void isr8();
extern void isr9();
extern void isr10();
extern void isr11();
extern void isr12();
extern void isr13();
extern void isr14();
extern void isr15();
extern void isr16();
extern void isr17();
extern void isr18();
extern void isr19();
extern void isr20();
extern void isr21();
extern void isr22();
extern void isr23();
extern void isr24();
extern void isr25();
extern void isr26();
extern void isr27();
extern void isr28();
extern void isr29();
extern void isr30();
extern void isr31();
extern void isr32();
extern void isr33();
extern void isr34();
extern void isr35();
extern void isr36();
extern void isr37();
extern void isr38();
extern void isr39();
extern void isr40();
extern void isr41();
extern void isr42();
extern void isr43();
extern void isr44();
extern void isr45();
extern void isr46();
extern void isr47();
extern void isr48();

typedef struct {
    unsigned short low_offset;
    unsigned short sel; /* kernel segment selector*/
    unsigned char always0;
    unsigned char flags;
    unsigned short high_offset;
} __attribute__((packed)) idt_gate_t;

typedef struct {
    unsigned short limit;
    unsigned int base;
} __attribute__((packed)) idt_register_t;

idt_gate_t idt[IDT_ENTRIES];
idt_register_t idt_reg;

void set_idt_gate(int n, unsigned int handler){
    idt[n].low_offset = (unsigned short)(handler & 0xffff);
    idt[n].sel = 0x08;
    idt[n].always0 = 0;
    idt[n].flags = 0x8e;
    idt[n].high_offset = (unsigned short)((handler >> 16) & 0xffff);
}

void set_idt() {
    idt_reg.base = (unsigned short)&idt;
    idt_reg.limit = IDT_ENTRIES * sizeof(idt_gate_t) - 1;
    __asm__ __volatile__("lidtl (%0)": : "r" (&idt_reg));
}

void io_wait(void) {
    __asm__ volatile (
    "jmp 1f;"
    "1:jmp 2f;"
    "2:"
    );
}

/* Input a byte from a port */
unsigned char inb(unsigned int port){
   unsigned char ret;
   __asm__ volatile ("inb %%dx,%%al":"=a" (ret):"d" (port));
   return ret;
}

/* Output a byte to a port */
void outb(unsigned int port, unsigned char value){
   __asm__ volatile ("outb %%al,%%dx": :"d" (port), "a" (value));
}

void set_pic(){

    unsigned char a1, a2;

	a1 = inb(PIC1_DATA);                        // save masks
	a2 = inb(PIC2_DATA);

	outb(PIC1_COMMAND, ICW1_INIT+ICW1_ICW4);  // starts the initialization sequence (in cascade mode)
	io_wait();
	outb(PIC2_COMMAND, ICW1_INIT+ICW1_ICW4);
	io_wait();
	outb(PIC1_DATA, NEW_OFFSET);                 // ICW2: Master PIC vector offset
	io_wait();
	outb(PIC2_DATA, NEW_OFFSET_2);                 // ICW2: Slave PIC vector offset
	io_wait();
	outb(PIC1_DATA, 4);                       // ICW3: tell Master PIC that there is a slave PIC at IRQ2 (0000 0100)
	io_wait();
	outb(PIC2_DATA, 2);                       // ICW3: tell Slave PIC its cascade identity (0000 0010)
	io_wait();

	outb(PIC1_DATA, ICW4_8086);
	io_wait();
	outb(PIC2_DATA, ICW4_8086);
	io_wait();

	outb(PIC1_DATA, a1);   // restore saved masks.
	outb(PIC2_DATA, a2);
}

void isr_install() {
    set_idt_gate(0,  (unsigned int)isr0);
    set_idt_gate(1,  (unsigned int)isr1);
    set_idt_gate(2,  (unsigned int)isr2);
    set_idt_gate(3,  (unsigned int)isr3);
    set_idt_gate(4,  (unsigned int)isr4);
    set_idt_gate(5,  (unsigned int)isr5);
    set_idt_gate(6,  (unsigned int)isr6);
    set_idt_gate(7,  (unsigned int)isr7);
    set_idt_gate(8,  (unsigned int)isr8);
    set_idt_gate(9,  (unsigned int)isr9);
    set_idt_gate(10, (unsigned int)isr10);
    set_idt_gate(11, (unsigned int)isr11);
    set_idt_gate(12, (unsigned int)isr12);
    set_idt_gate(13, (unsigned int)isr13);
    set_idt_gate(14, (unsigned int)isr14);
    set_idt_gate(15, (unsigned int)isr15);
    set_idt_gate(16, (unsigned int)isr16);
    set_idt_gate(17, (unsigned int)isr17);
    set_idt_gate(18, (unsigned int)isr18);
    set_idt_gate(19, (unsigned int)isr19);
    set_idt_gate(20, (unsigned int)isr20);
    set_idt_gate(21, (unsigned int)isr21);
    set_idt_gate(22, (unsigned int)isr22);
    set_idt_gate(23, (unsigned int)isr23);
    set_idt_gate(24, (unsigned int)isr24);
    set_idt_gate(25, (unsigned int)isr25);
    set_idt_gate(26, (unsigned int)isr26);
    set_idt_gate(27, (unsigned int)isr27);
    set_idt_gate(28, (unsigned int)isr28);
    set_idt_gate(29, (unsigned int)isr29);
    set_idt_gate(30, (unsigned int)isr30);
    set_idt_gate(31, (unsigned int)isr31);
    set_idt_gate(32, (unsigned int)isr32);
    set_idt_gate(33, (unsigned int)isr33);
    set_idt_gate(34, (unsigned int)isr34);
    set_idt_gate(35, (unsigned int)isr35);
    set_idt_gate(36, (unsigned int)isr36);
    set_idt_gate(37, (unsigned int)isr37);
    set_idt_gate(38, (unsigned int)isr38);
    set_idt_gate(39, (unsigned int)isr39);
    set_idt_gate(40, (unsigned int)isr40);
    set_idt_gate(41, (unsigned int)isr41);
    set_idt_gate(42, (unsigned int)isr42);
    set_idt_gate(43, (unsigned int)isr43);
    set_idt_gate(44, (unsigned int)isr44);
    set_idt_gate(45, (unsigned int)isr45);
    set_idt_gate(46, (unsigned int)isr46);
    set_idt_gate(47, (unsigned int)isr47);
    set_idt_gate(48, (unsigned int)isr48);

    set_idt(); // Load with ASM

    set_pic();
}

void isr_handler(unsigned int k){
    if(k == 32) {
        switcher();
    }
    if(k == 33){
        print(TO_PRINT);
    }
        /*
        else{
    	unsigned char* v = (unsigned char *) 0xb8000;
        unsigned char number[3];
        number[0] = k/10 + '0';
        number[1] = k%10 + '0';
        number[2] = '\n';
        print("\nInterrupt #");
        print(number);
    }
    */
}

#endif
