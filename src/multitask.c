#include "multitask.h"

int COUNT = 0;
int CURRENT_PROC;
context PROCESS[NUM_PROC+1];
const int ESP = 0x82800;

void start(char descriptor){
    CURRENT_PROC = descriptor;
    PROCESS[descriptor].is_in_queue ^= 1;
}

void load(context* con){
    /*Set esp*/
    __asm__ volatile (
        "movl %0, %%esp;"
        ::"r"(con->esp)
    );

    con->is_loaded = 1;

    /*Call process*/
    __asm__ volatile (
        "movl %0, %%eax;"
        "sti;"
        "call %%eax;"
        ::"p"(con->eip)
    );
}

void stop(int index){
    if(index < NUM_PROC)
        PROCESS[index].is_in_queue ^= 1;
}

void switcher() {
    context* proc;
    proc = &PROCESS[CURRENT_PROC];
    /*Save ebp*/
    __asm__ volatile (
            "movl %%ebp, (%0);"
            ::"p"(&proc->ebp)
            );
    int next_proc;
    for(next_proc = (CURRENT_PROC + 1)%NUM_PROC; next_proc != CURRENT_PROC; next_proc = (next_proc + 1)%NUM_PROC ){
        if (PROCESS[next_proc].is_in_queue)
            break;
    }

    if(next_proc == CURRENT_PROC && !PROCESS[next_proc].is_in_queue)
        next_proc = NUM_PROC; // waiter proccess

    proc = &PROCESS[next_proc];
    CURRENT_PROC = next_proc;

    outb(0xa0, 0x20);
    outb(0x20, 0x20);
    if(!proc->is_loaded)
        load(proc);
    else
        __asm__ volatile (
                "movl %0, %%ebp;"
                ::"r"(proc->ebp)
                );
}

void init_waiter(unsigned long int wairter_addr){
    context *proc = &PROCESS[NUM_PROC];
    proc->esp = ESP + COUNT * 0x2800;
    proc->ebp = proc->esp;
    proc->eip = wairter_addr;
}

context* init(unsigned long int start_addr){
    context *proc = &PROCESS[COUNT];
    proc->esp = ESP + COUNT * 0x2800;
    proc->ebp = proc->esp;
    proc->eip = start_addr;

    proc->is_in_queue = 1;
    proc->is_loaded = 0;
    COUNT++;
    return proc;
}
