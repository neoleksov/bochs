#ifndef MULT
#define MULT

#define NUM_PROC 3

typedef struct {
	unsigned long int esp;
	unsigned long int ebp;
	unsigned long int eip;
	unsigned long int is_loaded;
	unsigned long int is_in_queue;
} context;

void stop();
void load();
void switcher();
void init_waiter();
context* init();

#endif
