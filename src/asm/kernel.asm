[BITS 16]
;org 0x7e00
extern main

switch_to_pm:

  cli
  lgdt [gdt_descriptor]
  mov eax, cr0
  or  eax, 0x1
  mov cr0, eax

  jmp CODE_SEG:init_pm

%include "src/asm/gdt.asm"
[BITS 32]

init_pm:
  mov ax, 0x10
  mov ds,ax
  mov gs,ax
  mov ss,ax
  mov es,ax
  mov esp, 0x9000
  call main
