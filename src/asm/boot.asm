ORG 0x7c00

xor ax, ax
mov ds, ax
mov ss, ax
mov sp, 0xfffe

jmp 0x0:boot

boot:
    call load_disk
    jmp  0x0:0x7e00

load_disk:
    mov ah, 0x2
    mov al, 0x20
    
    xor cx, cx
    mov cl, 0x3
    
    xor dx, dx
    mov dl, 0x80
    
    xor bx, bx
    mov es, bx

    mov bx, 0x7e00
    int 0x13
ret



times 510-($-$$) db 0x00
db 0x55, 0xaa
