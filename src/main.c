#include "print.h"
#include "idt.h"
#include "multitask.h"
#include "proc.h"

void main(){
    clear();
    isr_install();

    init_waiter((unsigned long int)waiter);

    context* con = init((unsigned long int)proc1);
    init((unsigned long int)proc2);
    init((unsigned long int)proc3);
    __asm__("sti");

    load(con);
    for(;;);
}
