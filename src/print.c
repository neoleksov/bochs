#include "print.h"


int POS = 0;
unsigned char* vidmem =(unsigned char *) VIDEO_MEMORY;

void print_int(int k){
    char str[4];
    str[0] = k/10 + '0';
    str[1] = k%10 + '0';
    str[2] = '\n';
    str[3] = 0;
    print(str);
}

void print(char* str){
    for(int i = 0; str[i] != 0; ++i, POS += 2){
        if( str[i] == '\n' ){
            POS += 160 - 2 - POS % 160;
            continue;
        }
        if(POS > 4000){
            for(int i = 1; i <= 25; ++i)
                for(int j = 0; j <= 160; ++j)
                    vidmem[(i-1)*160 + j] = vidmem[i*160 + j];
            POS -= 160;
        }
        vidmem[POS] = str[i];
        vidmem[POS + 1] = 0x0f;
    }
}

void clear(){
    for(int i = 0; i <= 4000; i += 2){
        vidmem[i] = ' ';
        vidmem[i+1] = 0;
    }
}
