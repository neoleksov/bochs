#include "proc.h"
#include "print.h"

void proc1() {
    while (1) {
        TO_PRINT = "process 1\n";
        __asm__ volatile ("int $0x21");
        sleep();
    }
}

void proc2() {
    while (1) {
        TO_PRINT = "process 2\n";
        __asm__ volatile ("int $0x21");
        sleep();
    }
}

void proc3() {
    while (1) {
        TO_PRINT = "process 3\n";
        __asm__ volatile ("int $0x21");
        sleep();
    }
}

void sleep(){
    int i;
    int len = 999999;
    for(i = 0; i < len; ++i){}
}

void waiter(){
    while (1) {}
}
